// XPTloadouts.hpp
// Used for defining advanced respawn loadouts for players
// Default behaviour is to check if the player unit has a special loadout defined. Otherwise, it will check to see if the classname matches a loadout
// Advanced functionality allows mission creator to define exactly where items are placed in unit inventories
// Also supports sub-loadout randomization. If a loadout has sub-classes defined, the script will automatically select one of them to apply to the unit.
class loadouts
{
	class base
	{
		displayName = "Base Loadout";
		
		primaryWeapon[] = {};
		secondaryWeapon[] = {};
		handgunWeapon[] = {};
		binocular = "";
		
		uniformClass = "U_O_CombatUniform_ocamo";
		headgearClass = "";
		facewearClass = "";
		vestClass = "";
		backpackClass = "";
		
		linkedItems[] = {"ItemMap","ItemMicroDAGR","TFAR_fadak","ItemCompass","ItemWatch",""};
		
		uniformItems[] = {{"ItemcTabHCam",1}};
		vestItems[] = {};
		backpackItems[] = {};
		
		basicMedUniform[] = {{"ACE_fieldDressing",10},{"ACE_epinephrine",3},{"ACE_morphine",1},{"ACE_adenosine",2}};
		basicMedVest[] = {};
		basicMedBackpack[] = {};
		
		advMedUniform[] = {};
		advMedVest[] = {};
		advMedBackpack[] = {};
	};
	
	class O_officer_F: base
	{
		displayName = "CSAT Commander";

		primaryWeapon[] = {"arifle_Katiba_C_F","","acc_flashlight","optic_MRCO",{"30Rnd_65x39_caseless_green_mag_Tracer",30},{},""};
		secondaryWeapon[] = {};
		handgunWeapon[] = {"hgun_Pistol_heavy_02_F","","","",{"6Rnd_45ACP_Cylinder",6},{},""};
		binocular = "Rangefinder";

		uniformClass = "U_O_OfficerUniform_ocamo";
		headgearClass = "H_MilCap_ocamo";
		facewearClass = "G_Aviator";
		vestClass = "V_HarnessO_brn";
		backpackClass = "B_RadioBag_01_hex_F";
		//backpackClass = "B_AssaultPack_ocamo"; // If not enough people have Contact

		linkedItems[] = {"ItemMap","ItemcTab","TFAR_fadak","ItemCompass","ItemWatch",""};

		uniformItems[] = {{"ItemcTabHCam",1},{"6Rnd_45ACP_Cylinder",3,6}};
		vestItems[] = {{"ACE_EntrenchingTool",1},{"30Rnd_65x39_caseless_green_mag_Tracer",6,30},{"SmokeShell",2,1},{"SmokeShellRed",2,1},{"SmokeShellPurple",2,1}};
		backpackItems[] = {};

		basicMedVest[] = {};
		basicMedBackpack[] = {};
	};
	
	class cmdr_assist: base
	{
		displayName = "Commander's Assistant"; 
		
		primaryWeapon[] = {"arifle_Katiba_C_F","","acc_flashlight","optic_MRCO",{"30Rnd_65x39_caseless_green_mag_Tracer",30},{},""};
		secondaryWeapon[] = {};
		handgunWeapon[] = {"hgun_Pistol_heavy_02_F","","","",{"6Rnd_45ACP_Cylinder",6},{},""};
		binocular = "Rangefinder";

		uniformClass = "U_O_CombatUniform_ocamo";
		headgearClass = "H_HelmetO_ocamo";
		facewearClass = "G_Aviator";
		vestClass = "V_HarnessO_brn";
		backpackClass = "B_RadioBag_01_hex_F";
		//backpackClass = "B_AssaultPack_ocamo"; // If not enough people have Contact

		linkedItems[] = {"ItemMap","ItemcTab","TFAR_fadak","ItemCompass","ItemWatch",""};

		uniformItems[] = {{"ItemcTabHCam",1},{"6Rnd_45ACP_Cylinder",3,6}};
		vestItems[] = {{"ACE_EntrenchingTool",1},{"30Rnd_65x39_caseless_green_mag_Tracer",6,30},{"SmokeShell",2,1},{"SmokeShellRed",2,1},{"SmokeShellPurple",2,1}};
		backpackItems[] = {};

		basicMedVest[] = {};
		basicMedBackpack[] = {};

	};
	
	class crew_lead: base
	{
		displayName = "Lead Crewman"; 
		
		primaryWeapon[] = {"arifle_Katiba_C_F","","acc_flashlight","optic_ACO_grn",{"30Rnd_65x39_caseless_green_mag_Tracer",30},{},""};
		secondaryWeapon[] = {};
		handgunWeapon[] = {};
		binocular = "Rangefinder";

		uniformClass = "U_O_CombatUniform_ocamo";
		headgearClass = "H_HelmetCrew_O";
		facewearClass = "G_Bandanna_blk";
		vestClass = "V_HarnessO_brn";
		backpackClass = "TFAR_mr3000";

		linkedItems[] = {"ItemMap","ItemAndroid","TFAR_fadak","ItemCompass","ItemWatch",""};

		uniformItems[] = {{"ItemcTabHCam",1}};
		vestItems[] = {{"ACE_EntrenchingTool",1},{"30Rnd_65x39_caseless_green_mag_Tracer",6,30},{"SmokeShell",2,1},{"SmokeShellRed",2,1},{"SmokeShellPurple",2,1}};
		backpackItems[] = {{"ToolKit",1}};

		basicMedVest[] = {};
		basicMedBackpack[] = {};
	};
	
	class O_crew_F: base
	{
		displayName = "Crewman"; 
		
		primaryWeapon[] = {"arifle_Katiba_C_F","","acc_flashlight","optic_ACO_grn",{"30Rnd_65x39_caseless_green_mag_Tracer",29},{},""};
		secondaryWeapon[] = {};
		handgunWeapon[] = {};
		binocular = "Binocular";

		uniformClass = "U_O_CombatUniform_ocamo";
		headgearClass = "H_HelmetCrew_O";
		facewearClass = "G_Bandanna_blk";
		vestClass = "V_HarnessO_brn";


		uniformItems[] = {{"ItemcTabHCam",1}};
		vestItems[] = {{"ACE_EntrenchingTool",1},{"30Rnd_65x39_caseless_green_mag_Tracer",6,30},{"SmokeShell",2,1},{"SmokeShellRed",2,1},{"SmokeShellPurple",2,1}};

		basicMedVest[] = {};
	};
	
	class pilot_lead: base
	{
		displayName = "Pilot"; 
		
		primaryWeapon[] = {"SMG_02_F","","acc_flashlight","optic_ACO_grn_smg",{"30Rnd_9x21_Mag_SMG_02_Tracer_Green",30},{},""};
		secondaryWeapon[] = {};
		handgunWeapon[] = {};
		binocular = "Binocular";

		uniformClass = "U_O_PilotCoveralls";
		headgearClass = "H_PilotHelmetFighter_O";
		backpackClass = "TFAR_mr3000";

		linkedItems[] = {"ItemMap","ItemAndroid","TFAR_fadak","ItemCompass","ItemWatch",""};

		uniformItems[] = {{"ItemcTabHCam",1},{"30Rnd_9x21_Mag_SMG_02_Tracer_Green",1,30},{"SmokeShell",2,1},{"SmokeShellPurple",2,1}};
		backpackItems[] = {{"ToolKit",1},{"30Rnd_9x21_Mag_SMG_02_Tracer_Green",4,30}};

		basicMedBackpack[] = {};
	};
	
	class O_pilot_F: base
	{
		displayName = "Pilot";		
		
		primaryWeapon[] = {"SMG_02_F","","acc_flashlight","optic_ACO_grn_smg",{"30Rnd_9x21_Mag_SMG_02_Tracer_Green",30},{},""};
		secondaryWeapon[] = {};
		handgunWeapon[] = {};
		binocular = "Binocular";

		uniformClass = "U_O_PilotCoveralls";
		headgearClass = "H_PilotHelmetFighter_O";
		backpackClass = "B_AssaultPack_ocamo";

		uniformItems[] = {{"ItemcTabHCam",1},{"30Rnd_9x21_Mag_SMG_02_Tracer_Green",1,30},{"SmokeShell",2,1},{"SmokeShellPurple",2,1}};
		backpackItems[] = {{"ToolKit",1},{"30Rnd_9x21_Mag_SMG_02_Tracer_Green",4,30}};

		basicMedBackpack[] = {};
	};
	
	class O_Soldier_SL_F: base
	{
		displayName = "Squad Leader"; 
		
		primaryWeapon[] = {"arifle_Katiba_GL_F","","acc_flashlight","optic_MRCO",{"30Rnd_65x39_caseless_green_mag_Tracer",30},{"1Rnd_HE_Grenade_shell",1},""};
		secondaryWeapon[] = {};
		handgunWeapon[] = {"hgun_Rook40_F","","","",{"16Rnd_9x21_Mag",17},{},""};
		binocular = "Rangefinder";

		uniformClass = "U_O_CombatUniform_ocamo";
		headgearClass = "H_HelmetLeaderO_ocamo";
		vestClass = "V_HarnessOGL_brn";
		backpackClass = "TFAR_mr3000";
		//backpackClass = "B_AssaultPack_ocamo"; // If not enough people have Contact

		linkedItems[] = {"ItemMap","ItemAndroid","TFAR_fadak","ItemCompass","ItemWatch",""};

		uniformItems[] = {{"ItemcTabHCam",1},{"16Rnd_9x21_Mag",2,17}};
		vestItems[] = {{"ACE_EntrenchingTool",1},{"30Rnd_65x39_caseless_green_mag_Tracer",10,30}};
		backpackItems[] = {{"1Rnd_HE_Grenade_shell",7,1},{"1Rnd_SmokeRed_Grenade_shell",4,1},{"1Rnd_SmokePurple_Grenade_shell",4,1},{"1Rnd_Smoke_Grenade_shell",4,1},{"SmokeShellPurple",4,1},{"SmokeShellRed",4,1},{"SmokeShell",4,1},{"HandGrenade",2,1}};

		basicMedVest[] = {};
		basicMedBackpack[] = {};
	};
	
	class O_Soldier_TL_F: base
	{
		displayName = "Team Leader"; 

		primaryWeapon[] = {"arifle_Katiba_GL_F","","acc_flashlight","optic_MRCO",{"30Rnd_65x39_caseless_green_mag_Tracer",30},{"1Rnd_HE_Grenade_shell",1},""};
		secondaryWeapon[] = {};
		handgunWeapon[] = {"hgun_Rook40_F","","","",{"16Rnd_9x21_Mag",17},{},""};
		binocular = "Rangefinder";

		uniformClass = "U_O_CombatUniform_ocamo";
		headgearClass = "H_HelmetLeaderO_ocamo";
		vestClass = "V_HarnessOGL_brn";
		backpackClass = "B_FieldPack_ocamo";

		uniformItems[] = {{"ItemcTabHCam",1},{"16Rnd_9x21_Mag",2,17}};
		vestItems[] = {{"ACE_EntrenchingTool",1},{"30Rnd_65x39_caseless_green_mag_Tracer",11,30}};
		backpackItems[] = {{"1Rnd_HE_Grenade_shell",7,1},{"1Rnd_SmokeRed_Grenade_shell",4,1},{"1Rnd_SmokePurple_Grenade_shell",4,1},{"1Rnd_Smoke_Grenade_shell",4,1},{"SmokeShellPurple",4,1},{"SmokeShellRed",4,1},{"SmokeShell",4,1},{"HandGrenade",2,1}};

		basicMedVest[] = {};
		basicMedBackpack[] = {};
	};
	
	class O_medic_F: base
	{
		displayName = "Medic"; 
		
		primaryWeapon[] = {"arifle_Katiba_C_F","","acc_flashlight","optic_ACO_grn",{"30Rnd_65x39_caseless_green_mag_Tracer",30},{},""};
		secondaryWeapon[] = {};
		handgunWeapon[] = {"hgun_Rook40_F","","","",{"16Rnd_9x21_Mag",17},{},""};
		binocular = "Binocular";

		uniformClass = "U_O_CombatUniform_oucamo";
		headgearClass = "H_HelmetLeaderO_oucamo";
		vestClass = "V_HarnessO_gry";
		backpackClass = "B_Carryall_oucamo";

		uniformItems[] = {{"ItemcTabHCam",1},{"16Rnd_9x21_Mag",2,17}};
		vestItems[] = {{"ACE_EntrenchingTool",1},{"30Rnd_65x39_caseless_green_mag_Tracer",8,30},{"SmokeShell",2,1},{"SmokeShellRed",2,1},{"SmokeShellPurple",2,1},{"MiniGrenade",2,1}};
		backpackItems[] = {};

		basicMedVest[] = {};
		basicMedBackpack[] = {{"ACE_fieldDressing",90},{"ACE_bloodIV",10},{"ACE_epinephrine",30},{"ACE_morphine",20},{"ACE_adenosine",20}};
	};
	
	class O_Soldier_AR_F: base
	{
		displayName = "Autorifleman"; 
		
		primaryWeapon[] = {"LMG_Zafir_F","","acc_flashlight","optic_MRCO",{"150Rnd_762x54_Box_Tracer",150},{},""};
		secondaryWeapon[] = {};
		handgunWeapon[] = {"hgun_Rook40_F","","","",{"16Rnd_9x21_Mag",17},{},""};
		binocular = "Binocular";

		uniformClass = "U_O_CombatUniform_ocamo";
		headgearClass = "H_HelmetLeaderO_ocamo";
		vestClass = "V_HarnessO_brn";

		uniformItems[] = {{"ItemcTabHCam",1},{"16Rnd_9x21_Mag",2,17}};
		vestItems[] = {{"ACE_EntrenchingTool",1},{"150Rnd_762x54_Box_Tracer",2,150},{"SmokeShell",4,1},{"SmokeShellPurple",2,1},{"SmokeShellRed",2,1}};

		basicMedVest[] = {};
	};
	
	class O_Soldier_AAR_F: base
	{
		displayName = "Assistant Autorifleman"; 

		primaryWeapon[] = {"arifle_Katiba_F","","acc_flashlight","optic_MRCO",{"30Rnd_65x39_caseless_green_mag_Tracer",30},{},""};
		secondaryWeapon[] = {};
		handgunWeapon[] = {"hgun_Rook40_F","","","",{"16Rnd_9x21_Mag",17},{},""};
		binocular = "Binocular";

		uniformClass = "U_O_CombatUniform_ocamo";
		headgearClass = "H_HelmetLeaderO_ocamo";
		vestClass = "V_HarnessO_brn";
		backpackClass = "B_FieldPack_ocamo";

		uniformItems[] = {{"ItemcTabHCam",1},{"16Rnd_9x21_Mag",2,17}};
		vestItems[] = {{"ACE_EntrenchingTool",1},{"30Rnd_65x39_caseless_green_mag_Tracer",8,30},{"SmokeShell",4,1},{"SmokeShellPurple",4,1},{"SmokeShellRed",4,1},{"MiniGrenade",2,1}};
		backpackItems[] = {{"150Rnd_762x54_Box",4,150}};

		basicMedVest[] = {};
		basicMedBackpack[] = {};
	};
	
	class O_Soldier_AT_F: base
	{
		displayName = "Missile Specialist"; 

		primaryWeapon[] = {"arifle_Katiba_F","","acc_flashlight","optic_MRCO",{"30Rnd_65x39_caseless_green_mag_Tracer",30},{},""};
		secondaryWeapon[] = {"launch_O_Vorona_brown_F","","","",{"Vorona_HEAT",1},{},""};
		handgunWeapon[] = {"hgun_Rook40_F","","","",{"16Rnd_9x21_Mag",17},{},""};
		binocular = "Binocular";

		uniformClass = "U_O_CombatUniform_ocamo";
		headgearClass = "H_HelmetLeaderO_ocamo";
		vestClass = "V_HarnessO_brn";
		backpackClass = "B_FieldPack_ocamo";

		uniformItems[] = {{"ItemcTabHCam",1},{"16Rnd_9x21_Mag",2,17}};
		vestItems[] = {{"ACE_EntrenchingTool",1},{"30Rnd_65x39_caseless_green_mag_Tracer",8,30},{"SmokeShell",4,1},{"SmokeShellPurple",4,1},{"SmokeShellRed",4,1},{"MiniGrenade",2,1}};
		backpackItems[] = {{"Vorona_HEAT",2,1}};

		basicMedVest[] = {};
		basicMedBackpack[] = {};
	};
	
	class O_Soldier_AAT_F: base
	{
		displayName = "Assistant Missile Specialist"; 

		primaryWeapon[] = {"arifle_Katiba_F","","acc_flashlight","optic_MRCO",{"30Rnd_65x39_caseless_green_mag_Tracer",30},{},""};
		secondaryWeapon[] = {};
		handgunWeapon[] = {"hgun_Rook40_F","","","",{"16Rnd_9x21_Mag",17},{},""};
		binocular = "Binocular";

		uniformClass = "U_O_CombatUniform_ocamo";
		headgearClass = "H_HelmetLeaderO_ocamo";
		vestClass = "V_HarnessO_brn";
		backpackClass = "B_FieldPack_ocamo";

		uniformItems[] = {{"ItemcTabHCam",1},{"16Rnd_9x21_Mag",2,17}};
		vestItems[] = {{"ACE_EntrenchingTool",1},{"30Rnd_65x39_caseless_green_mag_Tracer",8,30},{"SmokeShell",4,1},{"SmokeShellPurple",4,1},{"SmokeShellRed",4,1},{"MiniGrenade",2,1}};
		backpackItems[] = {{"Vorona_HEAT",2,1}};

		basicMedVest[] = {};
		basicMedBackpack[] = {};
	};
	
	class O_Sharpshooter_F: base
	{
		displayName = "Sharpshooter"; 

		primaryWeapon[] = {"srifle_DMR_05_hex_F","","acc_flashlight","optic_KHS_hex",{"10Rnd_93x64_DMR_05_Mag",10},{},"bipod_02_F_hex"};
		secondaryWeapon[] = {};
		handgunWeapon[] = {"hgun_Rook40_F","","","",{"16Rnd_9x21_Mag",17},{},""};
		binocular = "Rangefinder";

		uniformClass = "U_O_CombatUniform_ocamo";
		headgearClass = "H_HelmetSpecO_ocamo";
		vestClass = "V_HarnessO_brn";
		backpackClass = "B_FieldPack_ocamo";

		uniformItems[] = {{"ItemcTabHCam",1},{"16Rnd_9x21_Mag",2,17}};
		vestItems[] = {{"ACE_EntrenchingTool",1},{"ACE_RangeCard",1},{"10Rnd_93x64_DMR_05_Mag",9,10}};
		backpackItems[] = {{"SmokeShell",4,1},{"SmokeShellPurple",4,1},{"SmokeShellRed",4,1},{"MiniGrenade",2,1}};

		basicMedVest[] = {};
		basicMedBackpack[] = {};
	};
	
	class O_Soldier_EXP_F: base
	{
		displayName = "Explosive Specialist"; 

		primaryWeapon[] = {"arifle_Katiba_F","","acc_flashlight","optic_MRCO",{"30Rnd_65x39_caseless_green_mag_Tracer",30},{},""};
		secondaryWeapon[] = {};
		handgunWeapon[] = {"hgun_Rook40_F","","","",{"16Rnd_9x21_Mag",17},{},""};
		binocular = "Binocular";

		uniformClass = "U_O_CombatUniform_ocamo";
		headgearClass = "H_HelmetLeaderO_ocamo";
		vestClass = "V_TacVest_khk";
		backpackClass = "B_AssaultPack_ocamo";

		uniformItems[] = {{"ItemcTabHCam",1},{"16Rnd_9x21_Mag",2,17}};
		vestItems[] = {{"ACE_EntrenchingTool",1},{"MineDetector",1},{"ACE_DefusalKit",1},{"30Rnd_65x39_caseless_green_mag_Tracer",6,30}};
		backpackItems[] = {{"30Rnd_65x39_caseless_green_mag_Tracer",2,30},{"SmokeShell",4,1},{"SmokeShellPurple",4,1},{"SmokeShellRed",4,1},{"MiniGrenade",2,1}};

		basicMedVest[] = {};
		basicMedBackpack[] = {};
	};
	
	class O_Soldier_F: base
	{
		displayName = "Rifleman"; 

		primaryWeapon[] = {"arifle_Katiba_F","","acc_flashlight","optic_MRCO",{"30Rnd_65x39_caseless_green_mag_Tracer",30},{},""};
		secondaryWeapon[] = {};
		handgunWeapon[] = {"hgun_Rook40_F","","","",{"16Rnd_9x21_Mag",17},{},""};
		binocular = "Binocular";

		uniformClass = "U_O_CombatUniform_ocamo";
		headgearClass = "H_HelmetLeaderO_ocamo";
		vestClass = "V_HarnessO_brn";

		uniformItems[] = {{"ItemcTabHCam",1},{"16Rnd_9x21_Mag",2,17}};
		vestItems[] = {{"ACE_EntrenchingTool",1},{"30Rnd_65x39_caseless_green_mag_Tracer",8,30},{"SmokeShell",4,1},{"SmokeShellPurple",4,1},{"SmokeShellRed",4,1},{"MiniGrenade",2,1}};

		basicMedVest[] = {};
	};
	
	class O_Soldier_A_F: base
	{
		displayName = "Ammo Bearer"; 

		primaryWeapon[] = {"arifle_Katiba_F","","acc_flashlight","optic_MRCO",{"30Rnd_65x39_caseless_green_mag_Tracer",30},{},""};
		secondaryWeapon[] = {};
		handgunWeapon[] = {"hgun_Rook40_F","","","",{"16Rnd_9x21_Mag",17},{},""};
		binocular = "Binocular";

		uniformClass = "U_O_CombatUniform_ocamo";
		headgearClass = "H_HelmetLeaderO_ocamo";
		vestClass = "V_HarnessO_brn";
		backpackClass = "B_Carryall_ocamo";

		uniformItems[] = {{"ItemcTabHCam",1},{"16Rnd_9x21_Mag",2,17}};
		vestItems[] = {{"ACE_EntrenchingTool",1},{"30Rnd_65x39_caseless_green_mag_Tracer",8,30},{"SmokeShell",4,1},{"SmokeShellPurple",4,1},{"SmokeShellRed",4,1},{"MiniGrenade",2,1}};
		backpackItems[] = {{"30Rnd_65x39_caseless_green_mag_Tracer",32,30}};

		basicMedVest[] = {};
		basicMedBackpack[] = {};
	};
	
	class logi_lead: base
	{
		displayName = "Logistics Coordinator"; 

		primaryWeapon[] = {"arifle_Katiba_C_F","","acc_flashlight","optic_MRCO",{"30Rnd_65x39_caseless_green_mag_Tracer",30},{},""};
		secondaryWeapon[] = {};
		handgunWeapon[] = {"hgun_Rook40_F","","","",{"16Rnd_9x21_Mag",17},{},""};
		binocular = "Rangefinder";

		uniformClass = "U_O_CombatUniform_ocamo";
		headgearClass = "H_HelmetLeaderO_ocamo";
		vestClass = "V_HarnessOGL_brn";
		backpackClass = "TFAR_mr3000";

		linkedItems[] = {"ItemMap","ItemAndroid","TFAR_fadak","ItemCompass","ItemWatch",""};

		uniformItems[] = {{"ItemcTabHCam",1},{"16Rnd_9x21_Mag",2,17}};
		vestItems[] = {{"ACE_EntrenchingTool",1},{"30Rnd_65x39_caseless_green_mag_Tracer",10,30}};
		backpackItems[] = {{"ToolKit",1}};

		basicMedVest[] = {};
		basicMedBackpack[] = {{"ACE_fieldDressing",40},{"ACE_bloodIV",2},{"ACE_epinephrine",10},{"ACE_morphine",5},{"ACE_adenosine",5}};
	};
	
	class O_engineer_F: base
	{
		displayName = "Combat Engineer"; 

		primaryWeapon[] = {"arifle_Katiba_C_F","","acc_flashlight","optic_ACO_grn",{"30Rnd_65x39_caseless_green_mag_Tracer",30},{},""};
		secondaryWeapon[] = {};
		handgunWeapon[] = {"hgun_Rook40_F","","","",{"16Rnd_9x21_Mag",17},{},""};
		binocular = "Binocular";

		uniformClass = "U_O_CombatUniform_ocamo";
		headgearClass = "H_HelmetLeaderO_ocamo";
		vestClass = "V_HarnessO_brn";
		backpackClass = "B_FieldPack_ocamo";

		uniformItems[] = {{"ItemcTabHCam",1},{"16Rnd_9x21_Mag",2,17}};
		vestItems[] = {{"ACE_EntrenchingTool",1},{"ACE_M26_Clacker",1},{"MineDetector",1},{"ACE_DefusalKit",1},{"30Rnd_65x39_caseless_green_mag_Tracer",7,30},{"SmokeShell",2,1},{"SmokeShellPurple",2,1},{"SmokeShellRed",2,1},{"HandGrenade",2,1}};
		backpackItems[] = {{"ToolKit",1},{"SatchelCharge_Remote_Mag",1,1},{"DemoCharge_Remote_Mag",2,1}};

		basicMedVest[] = {};
		basicMedBackpack[] = {};
	};
	
	class O_support_Mort_F: base
	{
		displayName = "Mortar Operator"; 

		primaryWeapon[] = {"arifle_Katiba_C_F","","acc_flashlight","optic_ACO_grn",{"30Rnd_65x39_caseless_green_mag_Tracer",30},{},""};
		secondaryWeapon[] = {};
		handgunWeapon[] = {"hgun_Rook40_F","","","",{"16Rnd_9x21_Mag",17},{},""};
		binocular = "Binocular";

		uniformClass = "U_O_CombatUniform_ocamo";
		headgearClass = "H_HelmetO_ocamo";
		vestClass = "V_HarnessO_brn";
		backpackClass = "TFAR_mr3000";

		uniformItems[] = {{"ItemcTabHCam",1},{"16Rnd_9x21_Mag",2,17}};
		vestItems[] = {{"ACE_EntrenchingTool",1},{"ACE_artilleryTable",1},{"30Rnd_65x39_caseless_green_mag_Tracer",8,30},{"SmokeShell",4,1},{"SmokeShellPurple",4,1},{"SmokeShellRed",4,1},{"MiniGrenade",2,1}};

		basicMedVest[] = {};
	};
};