// SXP_fnc_setupVehicle
// Handles setting up vehicles at mission start, and upon respawn

// Only run on the server
if (!isServer) exitWith {};

// Define variables
_this params [
	["_newVeh", nil, [objNull]],
	["_oldVeh", nil, [objNull]]
];

// If newVeh is nil, exit the script
if (isNil "_newVeh") exitWith {};

// Start our switch block, check the vehicle classname to determine what needs to be done for setup
switch (toLower (typeOf _newVeh)) do {
	// Mi-24
	case (toLower "CUP_O_Mi24_V_Dynamic_CSAT_T"):
	{
		// Configure pylons
		// Pylons in order from right wingtip to left wingtip
		_newVeh setPylonLoadout ["pylons1", "CUP_PylonPod_2Rnd_AT2_M", false, [0]]; // Right wingtip
		_newVeh setPylonLoadout ["pylons2", "PylonMissile_1Rnd_LG_scalpel", true, [0]];
		_newVeh setPylonLoadout ["pylons3", "CUP_PylonPod_250Rnd_TE2_Green_Tracer_GSh23_23mm_APHE_M", false, []];
		_newVeh setPylonLoadout ["pylons4", "CUP_PylonPod_250Rnd_TE2_Green_Tracer_GSh23_23mm_APHE_M", false, []];
		_newVeh setPylonLoadout ["pylons5", "PylonMissile_1Rnd_LG_scalpel", true, [0]];
		_newVeh setPylonLoadout ["pylons6", "CUP_PylonPod_2Rnd_AT2_M", false, [0]]; // Left wingtip
		
		_newVeh removeWeaponTurret ["CUP_Vmlauncher_S8_CCIP_veh",[-1]];
		_newVeh removeWeaponTurret ["CUP_Vmlauncher_AT6_veh",[0]];
		
		// Enable datalink
		_newVeh setVehicleReportRemoteTargets true;
		_newVeh setVehicleReceiveRemoteTargets true;
		_newVeh setVehicleReportOwnPosition true;
	};
};